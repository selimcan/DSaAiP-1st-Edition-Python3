
## C-1.20 Python's `random' module includes a function `shuffle(data)' that
## accepts a list of elements and randomly reorders the elements so that
## each possible order occurs with equal probability. The `randodm' module
## includes a more basic function `randint(a, b)' that returns a uniformly
## random integer from a to b (including both endpoints). Using only the
## `randint' function, implement your own version of the `shuffle' function.
##
## Goal runtime: O(n) Goal space: O(1)

import random

def shuffle_v1(data):
    """ List(Any) -> List(Any)
    Return a new list with elements randomly reordered so that each possible
    order occurs with equal probability.
    """
    if len(data) == 0 or len(data) == 1:
        return data
    else:
        res = []
        options = list(range(len(data)))
        while len(options) > 0:
            i = random.randint(0, len(options) - 1)
            res.append(data[options[i]])
            options.remove(options[i])
    return res
    ## Actual runtime: O(n^2) Actual space: O(n)


def shuffle_v2(data):
    """ List(Any) -> None
    Shuffle elements *in place* randomly so that each possible order occurs
    with equal probability (aka Fisher-Yates algorithm).
    """
    n = len(data)
    if n == 0 or n == 1:
        pass
    else:
        for i in range(n - 1, 0, -1):
            r = random.randint(0, i - 1)
            data[i], data[r] = data[r], data[i]
    ## Actual runtime: O(n) Actual space: O(1)

shuffle = shuffle_v2
