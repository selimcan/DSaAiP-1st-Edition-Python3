
## P-1.29 Write a Python program that outputs all possible strings formed by
## using the characters 'c', 'a', 't', 'd', 'o', and 'g' exactly once.
##
## Goal runtime: O(6!) Goal space: O(6!)

def permute_v1():  ## brute force
    """ None -> List(String)
    Output all possible strings formed by using the characters 'c', 'a', 't',
    'd', 'o', and 'g' exactly once.
    """

    res = []

    def p(choice, accum):
        if not choice:
            res.append(accum)
        else:
            for i in range(len(choice)):
                p(choice[:i] + choice[i+1:], accum + choice[i])

    p(['c', 'a', 't', 'd', 'o', 'g'], '')
    return res
    ## Actual runtime: O(6!) Actual space: O(6!)

def test_permute():
    assert len(list(permute_v1())) == 720
