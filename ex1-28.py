
## C-1.28 The p-norm of a vector v = (v_1, v_2, ..., v_n) in n-dimenstional
## space is defined as
##                      ||v|| = (v1^p + v2^p + ... + vn^p)^{1/p}
## For the special case of p = 2, this results in the traditional Eucliean
## norm, which represents the length of the vector. For example, the Euclidean
## norm of sqrt(4^2 + 3^2) = sqrt(16+9) = sqrt(25) = 5. Give an implementation
## of a function named nor such that norm(v, p) returns the p norm value of v
## and norm(v) returns the Euclidean norm of v. You may assume that v is a list
## of numbers.
##
## Goal runtime: O(n) Goal space: O(1)

from pytest import approx

def norm(v, p=2):
    """ List(Number) -> Number
    Return the p-norm value of v.
    """
    return sum(i**p for i in v)**(1/p)
    ## Actual runtime: O(n) Actual space: O(1)

def test_norm():
    assert norm([]) == 0
    assert norm([1]) == 1
    assert norm([4, 3]) == 5
    assert norm([3, 4], 3) == approx(4.49794144)
