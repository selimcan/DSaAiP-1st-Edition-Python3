
## P-1.30 Write a Python program that can take a positive integer greater
## than 2 as input and write out the number of times one must repeatedly
## divide this number by 2 before getting a value less than 2.
##
## Goal runtime: O(log(n)) Goal space: O(1)

import sys

def intlog2(n):
    """ Integer>2 -> Integer
    Given a positive integer greater than 2 return the number of times one must
    repeatedly divide this number by 2 before getting a value less than 2.
    """
    count = 0
    while n >= 2:
        n /= 2
        count += 1
    return count
    ## Actual runtime: O(log(n)) Actual space: O(1)

def test_intlog2():
    assert intlog2(3) == 1
    assert intlog2(4) == 2
    assert intlog2(5) == 2
    assert intlog2(6) == 2

if __name__ == "__main__":
    n = int(sys.stdin.read())
    print(intlog2(n))
