
## C-1.27 In Section 1.8, we provided three different implementations of a
## generator that computes factors of a given integer. The third of these
## implementations, from page 41 (shown below -- IFS), was the most efficient,
## but we noted that it did not yield the factors in increasing order. Modify
## the generator so that it reports factors in increasing order, while
## maintaining its general performance advantages.
##
## Goal runtime: O(sqrt(n)) Goal space: O(1)

def factors(n):
    k = 1
    while k * k < n:
        if n % k == 0:
            yield k
            yield n // k
        k += 1
    if k * k == n:
        yield k

def factors_inc(n):
    """ Int -> Generator(Int)
    Compute factors of a given integer and yield them in increasing order.
    """
    k = 1
    while k <= n:
        if n % k == 0:
            yield k
        k += 1
    ## Actual runtime: O(n) Actual space: O(1)
    ## That is, this one is probably a non-solution since it has a longer
    ## runtime (but the same space complexity)

def test_factors_inc():
    assert list(factors_inc(0)) == []
    assert list(factors_inc(1)) == [1]
    assert list(factors_inc(2)) == [1, 2]
    assert list(factors_inc(6)) == [1, 2, 3, 6]


def factors_inc_v2(n):
    """ Int -> Generator(Int)
    Compute factors of a given integer and yield them in increasing order.
    """
    if n == 1:
        yield 1
    else:
        factors = []
        k = 1
        while k * k <= n:
            if n % k == 0:
                yield k
                factors.append(n // k)
            k += 1
        for f in reversed(factors):
            yield f
    ## Actual runtime: O(sqrt(n)) Actual space: O(sqrt(n))
    ## That is, same time complexity, worse space complexity.

def test_factors_inc_v2():
    assert list(factors_inc_v2(0)) == []
    assert list(factors_inc_v2(1)) == [1]
    assert list(factors_inc_v2(2)) == [1, 2]
    assert list(factors_inc_v2(6)) == [1, 2, 3, 6]
