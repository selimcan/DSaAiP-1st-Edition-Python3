
## C-1.14 Write a short Python function that takes a sequence of integer values
## and determines if there is a distinct pair of numbers in the sequence whose
## product is odd.
##
## Goal runtime: O(n) Goal space: O(1)

def odd_product_v1(l):
    """ List(int) -> Boolean
    Retruen True if there is a distinct pair of numbers in the sequence whose
    product is odd.
    """
    if len(l) == 0 or len(l) == 1:
        return False
    for i in range(len(l)):
        for j in l[:i] + l[i+1:]:
            if l[i] * j % 2 == 1:
                return True
    return False
    # actual runtime: O(n^2) actual space: O(1)
    
def test_odd_product_v1():
    assert odd_product_v1([]) == False
    assert odd_product_v1([1]) == False
    assert odd_product_v1([1,2]) == False
    assert odd_product_v1([7,3]) == True
    assert odd_product_v1([7,4,2,5]) == True

    
## idea: a product of two numbers is odd iff both numbers are odd

def odd_product_v2(l):
    """ List(int) -> Boolean
    Retruen True if there is a distinct pair of numbers in the sequence whose
    product is odd.
    """
    if len(l) == 0 or len(l) == 1:
        return False
    found_first, found_second = False, False
    for n in l:
        if n % 2 == 1:
            if not found_first:
                found_first = True
            else:
                found_second = True
    return found_first and found_second
    # actual runtime O(n) actual space O(1)

def test_odd_product_v2():
    assert odd_product_v2([]) == False
    assert odd_product_v2([1]) == False
    assert odd_product_v2([1,2]) == False
    assert odd_product_v2([7,3]) == True
    assert odd_product_v2([7,4,2,5]) == True
