
## C-1.26 Write a short program that takes as input three integers, a, b and c,
## from the console and determines if they can be used in a correct arithmetic
## formula (in the given order), like ``a + b = c'', ``a = b - c'', or `` a * b
## = c''. 
##
## Goal runtime: O(1) Goal space: O(1)
