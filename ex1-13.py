
## C-1.13 Write a pseudo-code description of a function that reverses a list of
## n integers, so that the numbers are listed in the opposite order than they
## were before, and compare this method to an equivalent Python function for
## doing the same thing.
##
## Best conceivable runtime: O(n)

## Answer:
##
## I assume that reversing in place is required here. Given list is in the
## `data' variable.
##
## General idea: iterate over indices i from 0 to n // 2,
##               temp := data[i]
##               data[i] := data[n-i-1]
##               data[n-i-1] := temp
##


def reverse(l):
    """ List(int) -> List(int)
    Reverse a list of integers.
    """
    n = len(l)
    if n == 1:
        return l
    else:
        for i in range(n // 2):
            temp = l[i]
            l[i] = l[n - i - 1]
            l[n - i - 1] = temp
    return l
    ## runtime: O(n/2) = O(n)

def test_reverse():
    assert reverse([0]) == [0]
    assert reverse([0, 1]) == [1, 0]
    assert reverse([0, 1, 2]) == [2, 1, 0]
