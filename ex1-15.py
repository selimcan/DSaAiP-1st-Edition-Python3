
## C-1.15 Write a Python function that takes a sequence of numbers and
## determines if all the numbers are different from each other (that is, they
## are distinct).
##
## Goal runtime: O(n) Goal space: O(1)

def distinct(numbers):
    """ Iterable(Number) -> Boolean
    Return True if all numbers are different from each other (distinct).
    """
    return len(numbers) == len(set(numbers))
    ## actual runtime: O(n) actual space: O(n)

def test_distinct():
    assert distinct([]) == True
    assert distinct([1]) == True
    assert distinct([1,2]) == True
    assert distinct([2,2]) == False
    assert distinct([2, 2.0]) == False
    assert distinct([2,23,5]) == True

## If necessary, O(1) space can be achieved by iterating over the numbers
## of the list, and searching for the same number in the `remaining' part
## of the list (left slice + right slice).
