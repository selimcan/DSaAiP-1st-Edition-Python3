
## C-1.22 Write a short Python program that takes two arrays a and b of length
## n storing int values, and returns the dot product of a and b. That is, it
## returns an array c of length n such that c[i] = a[i] * b[i], for i = 0, ...,
## n-1.
##
## Goal runtime: O(n) Goal space: O(1)


def dot(a, b):
    """ List(Int) List(Int) -> Generator(Int)
    Return dot product of a and b.
    """
    if len(a) != len(b):
        raise ValueError("Lists must be of equal length.")
    else:
        for m, n in zip(a, b):
            yield m * n
    ## Actual runtime: O(n) Actual space: O(1)

def test_dot():
    assert list(dot([], [])) == []
    assert list(dot([1], [-2])) == [-2]
    assert list(dot([-3, 4], [-2, 4])) == [6, 16]


def dot_v2(a, b):
    """ List(Int) List(Int) -> Generator(Int)
    Return dot product of a and b.
    """
    if len(a) != len(b):
        raise ValueError("Lists must be of equal length.")
    else:
        return (m * n for m, n in zip(a, b)) ## a `generator comprehension',
                                             ## I presume
    ## Actual runtime: O(n) Actual space: O(1)

def test_dot_v2():
    assert list(dot_v2([], [])) == []
    assert list(dot_v2([1], [-2])) == [-2]
    assert list(dot_v2([-3, 4], [-2, 4])) == [6, 16]
