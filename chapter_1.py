## R-1.1 Write a short Python function, is_multiple(n, m), that takes two
## integer values and returns True if n is a multiple of m, that is, n = mi for
## some integer i, and False otherwise.

import pytest

def is_multiple_version_1(m, n):
    """ Int Int -> Boolean
    Return True if n is a multiple of m, that is, n = mi for some integer i,
    and False otherwise.
    """
    if not isinstance(m, int) or not isinstance(n, int):
        raise ValueError('Both arguments must be integers.')
    else:
        return n % m == 0

def test_is_multiple_version_1():
    with pytest.raises(ValueError, match='Both arguments must be integers.'):
        assert is_multiple_version_1(1.0, 2)
    with pytest.raises(ValueError, match='Both arguments must be integers.'):
        assert is_multiple_version_1(1, 2.0)
    with pytest.raises(ValueError, match='Both arguments must be integers.'):
        assert is_multiple_version_1(1.0, 2.0)
    assert is_multiple_version_1(-1, 5) == True
    assert is_multiple_version_1(-2, -6) == True
    assert is_multiple_version_1(2, 5) == False

def is_multiple_version_2(m, n):
    """ Int Int -> Boolean
    Return True if n is a multiple of m, that is, n = mi for some integer i,
    and False otherwise.
    """
    return n % m == 0

def test_is_multiple_version_2():
    assert is_multiple_version_2(-1, 5) == True
    assert is_multiple_version_2(-2, -6) == True
    assert is_multiple_version_2(2, 5) == False

is_multiple = is_multiple_version_2


## R-1.2 Write a short Python function, is_even(k), that takes an integer value
## and returns True if k is even, and False otherwise. However, your function
## cannot use the multiplication, modulo, or division operators.

## Idea: look at the last digit
## Best conceivable runtime: O(1)

def is_even(k):
    """ Int -> Boolean
    Return True if k is even, and False otherwise.
    """
    return str(k)[-1] in ['0', '2', '4', '6', '8']
    ## actual runtime: O(log(k))

def test_is_even():
    assert is_even(-2) == True
    assert is_even(-1) == False
    assert is_even(0) == True
    assert is_even(101) == False
    assert is_even(12234) == True


## R-1.3 Write a short Python function, minmax(data), that takes a sequence of
## one or more numbers, and returns the smallest and largest numbers, in the
## form of a tuple of length two. Do not use the built-in functions min or max
## in implementing your solution.

## Best conceivable runtime: O(n)

def minmax(data):
    """ Iterable(Number) -> (Number, Number)
    Return the smallest and largest numbers from the iterable `data'.
    ASSUME: len(data) >= 1
    """
    smallest, largest = None, None
    for n in data:
        if not smallest or n < smallest:
            smallest = n
        if not largest or n > largest:
            largest = n
    return smallest, largest
    ## actual runtime: O(n)

def test_minmax():
    assert minmax([1]) == (1, 1)
    assert minmax([5, -0.23]) == (-0.23, 5)
    assert minmax([-0.23, 3.34, 1, -45]) == (-45, 3.34)


## R-1.4-5 Write a short Python function that takes a positive integer n and
## returns the sum of the squares of all the postive integers smaller than n.

## Best conceivable runtime: O(1) (if re-written into an explicit formula)
##                           O(n) (if not)

def sum_squares(n):
    """ PositiveInt -> PositiveInt
    Return the sum of the squares of all the positive integers smaller than n.
    """
    return sum(x * x for x in range(n))
    ## runtime: O(n)

def test_sum_squares():
    assert sum_squares(1) == 0
    assert sum_squares(2) == 1
    assert sum_squares(4) == 14


## R-1.6-7 Write a short Python function that takes a positive integer n and
## returns the sum of the squares of all the odd positive integers smaller
## than n.

## Best conceivable runtime: O(n)

def sum_squares_odd(n):
    """ PositiveInt -> PositiveInt
    Sum squares of all the odd numbers smaller than n.
    """
    return sum(x * x for x in range(n) if x % 2 == 1)

def test_sum_squares_odd():
    assert sum_squares_odd(1) == 0
    assert sum_squares_odd(2) == 1
    assert sum_squares_odd(5) == 10


## R-1.8 Python allows negative integers to be used as indices into a sequence,
## such as a string. If string s has length n, and expression s[k] is used for
## index -n <= k < 0, what is the equivalent index j >= 0 such that s[j] refe-
## rences the same element?
##
## Answer:
## if k = -1, then j = len(s) - 1
## if k = -2, then j = len(s) - 2
## etc, therefore j = len(s) + k = n + k


## R-1.9 What parameters should be sent to the range constructor, to produce
## a range with values 50, 60, 70, 80?
##
## Answer: range(50, 90, 10)



