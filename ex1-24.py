
## C-1.24 Write a short Python function that counts the number of vowels in a
## given character string.
##
## Goal runtime: O(n) Goal space: O(1)

def count_vowels(s):
    """ String -> Int
    Count the number of vowels in the given character string.
    """
    return sum(1 for c in s if c in 'aeiou')
    ## Actual runtime: O(n) Actual space: O(1)

def test_count_vowels():
    assert count_vowels("") == 0
    assert count_vowels("abcdefghijklmnopqrstufvxyz") == 5
