
## C-1.25 Wrtie a short Python function that takes a string s, representing a
## sentence, and returns a copy of the string with all punctuation removed. For
## example, if given the string "Let's try, Mike.", this function would return
## "Lets try Mike".
##
## Goal runtime: O(n) Goal space: O(n)

from string import punctuation

def remove_punctuation(s):
    """ String -> String
    Return a copy of the given character string with all punctuation removed.
    """
    return ''.join(c for c in s if not c in punctuation)
    ## Actual runtime: O(n) Actual space: O(n)

def test_remove_punctuation():
    assert remove_punctuation("Let's try, Mike.") == "Lets try Mike"
