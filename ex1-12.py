
from random import randrange

def my_choice(data):
    """ Sequence -> Any
    Return a random element from a non-empty sequence.
    """
    return data[randrange(len(data))]
