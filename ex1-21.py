
## C-1.21 Write a Python program that repeatedly reads lines from standard
## input until an EOFError is raised, and then outputs those lines in reverse
## order (a user can indicate end of input by typng ctrl-D).
##
## Goal runtime: O(n) Goal space: O(n)

mem = []

while True:
    try:
        line = input('Type your input: ')
    except EOFError:
        break
    mem.append(line)

print('\n')
for line in reversed(mem):
    print(line)


